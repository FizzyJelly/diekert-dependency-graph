from __future__ import annotations

import logging
from typing import Dict, Sequence, Set, Tuple, Union


class Relation:

    __slots__ = ['elements', 'relation', 'name', '_logger']

    def __init__(self, name: str, elements: Sequence[str],
                 relations: Union[Sequence[Tuple[str, str]], Dict[str, Set[str]]]):
        self._logger = logging.getLogger(f"{self.__class__.__name__}")

        self.elements = elements
        if isinstance(relations, Sequence):
            self.relation = {a: set() for a in elements}
            for a, b in relations:
                self.relation[a].add(b)
        elif isinstance(relations, Dict):
            self.relation = {a: {b for b in other}
                             for a, other in relations.items()}
        else:
            self._logger.error(f"Bad relation input type: {type(relations)}\n"
                               f"Dict or tuple list expected instead...")
            raise RuntimeError(
                f"Unsipported relation input type: {type(relations)}")

        for a in self.relation:
            if a not in self.elements:
                self._logger.error(f"Element {a} from {self.relation} was not declared "
                                   f"in relation elements...")
                raise RuntimeError(
                    f"Element {a} from relation was not specified in relation elements")

        self.name = name

    @property
    def tuple_list(self):
        return [(a, b) for a in self.elements for b in self.relation[a]]

    def __str__(self):
        res = f"{self.name} relation:\n"
        for a, other in self.relation.items():
            res += f"{a}:\n"
            res += f"  {','.join(other)}\n"

        return res

    def __iter__(self):
        for item in self.relation.items():
            yield item

    def __getitem__(self, key) -> Set[str]:
        return self.relation[key]


class Independence(Relation):

    def __init__(self, elements: Sequence[str],
                 relations: Union[Sequence[Tuple[str, str]], Dict[str, Set[str]]]):
        super().__init__('Independence', elements, relations)

    @staticmethod
    def from_dependence(dependence: Dependence) -> Independence:
        dependence._logger.debug(
            f"Calculating independence relation based on: \n{dependence}")
        indep = {a: {b for b in dependence.elements if b != a}
                 for a in dependence.elements}
        for a, other in dependence:
            for b in other:
                if b in indep[a]:
                    indep[a].remove(b)
        indep = Independence(dependence.elements, indep)
        dependence._logger.debug(f"Resulting independence:\n{indep}")
        return indep


class Dependence(Relation):

    def __init__(self, elements: Sequence[str],
                 relations: Union[Sequence[Tuple[str, str]], Dict[str, Set[str]]]):
        super().__init__('Dependence', elements, relations)

    @staticmethod
    def from_independence(independence: Independence) -> Dependence:
        independence._logger.debug(
            f"Calculating dependence relation based on: \n{independence}")
        dep = {a: {b for b in independence.elements}
               for a in independence.elements}
        for a, other in independence:
            for b in other:
                if b in dep[a]:
                    dep[a].remove(b)
        dep = Dependence(independence.elements, dep)
        independence._logger.debug(f"Resulting dependence:\n{dep}")
        return dep
