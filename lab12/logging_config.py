import logging
import logging.config
from traceback import print_exc, print_tb

import yaml


def from_file(f_name: str, level=logging.INFO):
    try:
        with open(f_name, 'r') as f:
            config = yaml.safe_load(f.read())
            logging.config.dictConfig(config)
            logging.root.level = level
    except Exception as e:
        print_exc(e)
