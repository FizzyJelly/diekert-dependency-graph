import logging
from pprint import pformat
from typing import Dict, List, Set

from .relations import Dependence
from .trace import FoataForm, Trace


class Node:

    __slots__ = ['label']

    def __init__(self, label: str):
        self.label: str = label

    def __hash__(self):
        return hash(id(self))

    def __eq__(self, other):
        return isinstance(other, Node) and id(self) == id(other)

    def __str__(self) -> str:
        return f"Node: {self.label}"

    def __repr__(self) -> str:
        return self.label


class Graph:

    __slots__ = ['nodes', 'name', '_logger']

    def __init__(self, name):
        self.nodes: Dict[Node, Set[Node]] = {}
        self.name: str = name
        self._logger: logging.Logger = logging.getLogger(
            f"{self.__class__.__name__}")

    def add_node(self, node: Node) -> None:
        if node not in self.nodes:
            self.nodes[node] = []
        else:
            raise RuntimeError(f"{node} already exists in {self.name}")

    def add_edge(self, u: Node, v: Node) -> None:
        if u not in self.nodes:
            self._logger.debug(f"Cannot add edge: {repr(u)} -> {repr(v)}, "
                               f"{u} not present in graph nodes...")
            raise RuntimeError(f"Cannot add edge: {repr(u)} -> {repr(v)}")
        self.nodes[u].append(v)

    def remove_edge(self, u: Node, v: Node) -> None:
        if u not in self.nodes:
            self._logger.debug(f"Cannot remove edge: {repr(u)} -> {repr(v)}, "
                               f"{u} not present in graph nodes...")
            raise RuntimeError(f"Cannot remove edge: {repr(u)} -> {repr(v)}")
        if v not in self.nodes[u]:
            self._logger.debug(f"Cannot remove edge: {repr(u)} -> {repr(v)}, "
                               f"does not exist in graph...")
            raise RuntimeError(f"Cannot remove edge: {repr(u)} -> {repr(v)}")

        self.nodes[u].remove(v)

    def neighbours(self, u: Node) -> Set[Node]:
        return self.nodes[u]

    def __iter__(self):
        return iter(self.nodes.keys())

    def __str__(self) -> str:
        return f"Graph: {self.name}\n Adjacency lists:\n{pformat(self.nodes)}"


class DependenceGraph(Graph):

    def __init__(self, trace: Trace):
        super().__init__("Dependence diagram")
        self.trace: Trace = trace
        self.foata_first_group: Set[Node] = Node
        self.__build_hasse_diagram()

    def __build_hasse_diagram(self):
        self._logger.debug(f"Building diagram for: {self.trace} "
                           f"with given: {self.trace.dependence}")

        min_set: Set[Node] = set()
        present_nodes: List[Node] = []
        for c in reversed(self.trace.word):
            new_node = Node(c)
            min_set.add(new_node)
            present_nodes.append(new_node)
            self.add_node(new_node)

            to_remove = set()
            for node in present_nodes:
                if node != new_node:
                    if node.label in self.trace.dependence[new_node.label]:
                        self.add_edge(new_node, node)
                        if node in min_set:
                            to_remove.add(node)

            for node in to_remove:
                min_set.remove(node)

        to_remove = []
        for x in self:
            for y in self.neighbours(x):
                for z in self.neighbours(y):
                    if z in self.neighbours(x):
                        to_remove.append((x, z))

        for u, v in to_remove:
            if v in self.neighbours(u):
                self.remove_edge(u, v)

        self.foata_first_group = tuple(
            sorted(min_set, key=lambda node: node.label))
        self._logger.debug(f"Resulting graph:\n{self}")

    @property
    def foata_form(self) -> FoataForm:
        self._logger.debug(
            f"Calculating Foata normal form based on {self.name} for {self.trace}")
        groups = []
        excluded_edges = set()

        group = self.foata_first_group
        while len(group) > 0:
            groups.append([node for node in group])
            next_group = []
            for node in group:
                for neigh in self.neighbours(node):
                    excluded_edges.add((node, neigh))
                    check = [(src, neigh) for src in self if src not in group and neigh in self.neighbours(
                        src) and (src, neigh) not in excluded_edges]

                    if len(check) == 0:
                        if neigh not in next_group:
                            next_group.append(neigh)

            group = tuple(sorted(next_group, key=lambda node: node.label))

        foata = FoataForm(groups)
        self._logger.debug(f"Resulting foata form: {foata}")
        return foata
