import argparse
import logging

from . import example, logging_config

parser = argparse.ArgumentParser()

parser.add_argument('--logging-level', default=logging.DEBUG)
parser.add_argument('--logging-config', default='./lab12/logging.yml')

args = vars(parser.parse_args())

logging_config.from_file(args['logging_config'], args['logging_level'])


example.run_example_1()
example.run_example_2()
