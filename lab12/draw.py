from random import random

import matplotlib.pyplot as plt
import networkx as nx

from .graph import DependenceGraph
from .trace import FoataForm


class DependenceDrawer:

    def __init__(self, graph: DependenceGraph):
        self.graph: DependenceGraph = graph
        self.foata: FoataForm = graph.foata_form

    def draw_and_save(self, filename='./dependence_graph.png'):
        plt.close('all')
        G = nx.DiGraph()
        horizontal = 0
        vertical = 0
        vertex_num = 0
        node_mapping = {}
        for group in self.foata.groups:
            vertical = 0
            for node in group:
                G.add_node(vertex_num, pos=(
                    horizontal + random()/5 - 0.1, vertical + random()/3 - 0.3), label=node.label)
                node_mapping[node] = vertex_num
                vertex_num += 1
                vertical += 1

            horizontal += 1

        for node in self.graph:
            for neigh in self.graph.neighbours(node):
                G.add_edge(node_mapping[node], node_mapping[neigh])

        nx.draw(G, nx.get_node_attributes(G, 'pos'), node_size=400,
                labels=nx.get_node_attributes(G, 'label'))
        plt.savefig(filename)
