import logging
from pprint import pformat
from typing import Any, Sequence

from .relations import Dependence, Independence


class FoataForm:

    __slots__ = ['groups']

    def __init__(self, groups: Sequence[Sequence[Any]]):
        self.groups = groups

    def __str__(self):
        res = ""
        for group in self.groups:
            res += f"({','.join([str(e) for e in group])})"

        return res


class Trace:

    __slots__ = ['word', 'independence', 'dependence', '__logger']

    def __init__(self, word: str, actions_independence: Independence):
        self.word: str = word
        self.independence: Independence = actions_independence
        self.dependence: Dependence = Dependence.from_independence(
            self.independence)
        self.__logger = logging.getLogger(
            f"{self.__class__.__name__}")

        check = [c for c in word if c not in self.independence.elements]
        if len(check) > 0:
            self.__logger.error(
                f"{self} contains elements not specified in independence relation: {check}")
            raise RuntimeError(
                f"Trace contains elements that are not specified in provided relation...\n"
                f"Mismatched elements: {check}")

    @property
    def foata_form(self):

        self.__logger.debug(f"Calculating Foata normal form for {self} "
                            f"with given {self.dependence}")

        marker = '*'
        letter = '|'

        def stacks_empty(stacks):
            for _, stack in stacks.items():
                if len(stack) > 0:
                    return False

            return True

        stacks = {a: [] for a in self.independence.elements}
        for c in reversed(self.word):
            stacks[c].append(letter)
            for dep in self.dependence[c]:
                if dep != c:
                    stacks[dep].append(marker)
        self.__logger.debug(f"Foata calculation stacks: \n{pformat(stacks)}\n")

        groups = []
        while not stacks_empty(stacks):
            group = []
            to_pop = {a: 0 for a in stacks.keys()}
            for elem, stack in stacks.items():
                if len(stack) > 0 and stack[-1] == letter:
                    group.append(elem)
                    for dep in self.dependence[elem]:
                        to_pop[dep] = to_pop[dep] + 1

            for elem, number in to_pop.items():
                if number != 0:
                    stacks[elem] = stacks[elem][:-number]

            groups.append(sorted(group))

        foata = FoataForm(groups)
        self.__logger.debug(f"Foata normal form for {self} is {foata}")

        return foata

    def __str__(self):
        return f"Trace: [{self.word}]"
