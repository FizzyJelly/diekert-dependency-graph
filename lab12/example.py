from .relations import Dependence, Independence
from .draw import DependenceDrawer
from .graph import DependenceGraph
from .trace import Trace


def run_example_1():
    elements = ['a', 'b', 'c', 'd']
    independence_relation = [('a', 'd'), ('d', 'a'), ('b', 'c'), ('c', 'b')]
    trace = Trace('baadcb', Independence(elements, independence_relation))
    foata = trace.foata_form
    diekerts_graph = DependenceGraph(trace)
    DependenceDrawer(diekerts_graph).draw_and_save("./example_1.png")


def run_example_2():
    elements = ['a', 'b', 'c', 'd', 'e', 'f']
    independence_relation = [('a', 'd'), ('d', 'a'), ('b', 'e'),
                             ('e', 'b'), ('c', 'd'), ('d', 'c'), ('c', 'f'), ('f', 'c')]
    trace = Trace('acdcfbbe', Independence(elements, independence_relation))
    foata = trace.foata_form
    diekerts_graph = DependenceGraph(trace)
    DependenceDrawer(diekerts_graph).draw_and_save("./example_2.png")
